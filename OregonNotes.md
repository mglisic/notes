Oregon Notes (Craig Gallagher, Marija Glisic, remote FW/SW help from Ismet Siral, and Laura Jeanty)
---------------------------------------------------------------

Reception Inspection:
---------
- Module was received in good condition.  Pictures of the module are included.
- Visual inspection found no scratches or dents, and the wire bond connections looked good. There was some white stuff on the backside (see photo) but it looks innocuous and was there in photos from previous sites.
- Jumper configuration was checked. IREF_TRIM was set with two jumpers in the middle two pins, configuration 4b,0110. This matched the notes and photo from Gottingen.  Other jumpers matched the SCC configuration document for the LDO setup.  IREF_TRIM was left as is.
- We didn't have a precise current measuring device on hand and the scary red text gave us pause, so we skipped trimming IREF and planned to come back to it only if tunings didn't work. Tunings worked, so we didn't come back.
- Our YARR setup got its final HW pieces right before the module arrived; the timing was perfect. Also worth noting how the YARR setup worked on the FE on the first try (and this was the first FE tested). Amazing how plug-n-play it is. 
- We trimmed the internal voltage (VDDD and VDDA) twice when the chip stopped sending valid data. Both times adjusting SldoDigitalTrim and SldoAnalogTrim registers fixed it, but somewhat mysteriously. In the config, both were originally set to 22. When the chipped stopped sending data the first time, the voltage across both the VDDD and VDDA pins was about 1.1 V. We increased both registers to 26, at which point the voltage went to 1.3, and the chipped starting working again. We returned the registers to 22, and the voltage went to 1.2 V, where we left it. The second time the chip stopped sending data, we tried again going to 26, but the voltage stayed at 1.1V. We went down to 16, which seemed to put the chip in a happy communication state, and then returned to 22 where voltage was again 1.2 and it continued working.

Scans:
-----
- Our YARR release was version 1.0.0 on master.
- We started with the config file linked on the travelling module page (from Lingxin).
- On power-up the chip drew 0.48 A and fluctuated by 0.3 A.  After first digital scan current jumped to 0.62 A and settled there. We recorded current consumption during scans; noted that it drops when tuning / scanning only one of three FE, and increases when you enable all pixels for the noise scan.
- Resistance of the NTC was measured a few times during scanning with a multimeter. Measurements were 7.6 kOhms which correspond to a temperature of 32 degrees C.  The lab's temperature was around 23 degrees C.

The scans below are noted by (data set), description, and the arguments used.  Data sets not noted here were either from debugging the chip communication problem mentioned in reception inspection, playing around with tuning the differential front-end (though we didn't end up with a particularly good tuning in the end), or from threshold scans between tunings to see the effects of each tuning.

Pre-tune Scans:
- (7) digital scan, all FE
- (8) analog scan, all FE
- (9) threshold scan, all FE
- (10) ToT scan, 10k e-: '-t 10000'

Linear Tuning:
- (60) lin global threshold tune, 2ke-: '-t 2000'
- (61) lin pixel threshold tune, 2ke-: '-t 2000'
- (62) lin global threshold retune, 1ke-: '-t 1000'
- (63) lin pixel threshold retune, 1ke-: '-t 1000'
- (65) lin global preamp tune, 10ke- and ToT 8bc: '-t 10000 8'
- (66) lin pixel threshold retune, 1ke-: '-t 1000'

Synchronous Tuning:
- (68) syn global threshold tune, 1ke-: '-t 1000'
- (69) syn global preamp tune, 10ke- and ToT 8bc: '-t 10000 8'
- (70) syn global threshold tune, 1ke-: '-t 1000'

Differential Tuning:
- (76) diff global threshold tune, 1ke-: '-t 1000'
- (77) diff pixel threshold tune, 1ke-: '-t 1000'
- (78) diff global preamp tune, 10ke- and ToT 8bc: '-t 10000 8'
- (79) diff pixel threshold tune, 1ke-: '-t 1000'    
*First tried tuning the differential FE using the pixel retuning script, but this made the threshold worse. Redid the diff tune replacing the pixel retune with just the pixel tune. 

Post-tune Scans:
- (81) ToT scan, 10k e-: '-t 10000'
- (85) Noise scan, all FE
- (87) threshold scan, all FE

The threshold, noise, and ToT plotting scripts were run on the initial and final threshold and ToT scans (data sets 9, 10, 81, and 87) 