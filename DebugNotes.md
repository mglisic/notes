Background:
-----------
Last month (May 3rd) I introduced the rd53a test stand (TS) to Laura Nosler, ran her through how things were setup, how scanning the chip worked, and did an entire tuning routine for practise (on the differential part of the chip).  Things worked great, cut to Friday, Jun 4th, introducing the test stand to Nathan Young, things did not work great.  

Friday, Jun 4th:
----------------
Upon arrival, no visible changes to TS, power channel to chip was off.
There is a labremote system created by Ismet Siral (https://gitlab.cern.ch/isiral/labremote-dcs-oregon) to control the chip's power system from afar.  Available scripts are:

- StartDCS: turns the channel on, outputs initial voltage and current reading, asks if user wishes to proceed. If  abnormal current or voltage is observed then power channel is turned off (abnormal means current is above a max value (then 0.7A), or voltage more than 0.2V off from 1.8V).
- CheckDCS: monitors the voltage and current from the power channel.  Every 3 seconds, the voltage and current are printed out, and if abnormal current or voltage observed, channel is turned off.
- CloseDCS: turns off channel, checks that voltage and current are both 0.

First tried to run StartDCS, but it read an abnormal current (0.83A), and would not turn on.  
Removing MOLEX power cables from chip, turning power system off and on, then plugged power cable back into chip (without turning on power to the chip).  Then running StartDCS worked, with the usual 0.56A current.
Ran std_digitalscan, got a very blocky Occupancy map ([https://gitlab.cern.ch/mglisic/notes/-/blob/master/JohnDoe_0_OccupancyMap.png](url)).  Sadly, did not realize there exited a similar example in the YARR troubleshooting documentation til much later.

Tuesday, Jun 8th:
-----------------
Met with Ismet remotely (me in the lab, Ismet at CERN) to try debugging, focusing mainly on the power system. With the help of a multimeter, found that:
- When chip is unplugged from power source, channel 1 of power system is suppplying 1.8V (as is channel 2, though channel 1 is typically what powers the chip). -> Doesn't seem to be a problem with the power source's voltage.
- At chip-level, recieves 1.72V across the PWR IN pins.
- Comparing the Digital power input to the Analog power input at the board, we actually observe a 1.2V difference there
- Turned off computer and power source, still same problem with abnormal current.

Also checked that jumper configuration is set up as desribed in [https://yarr.web.cern.ch/yarr/](url).
Changed maxCurrent to 1.0A (for abnormal current threshold)

Wednesday, Jun 9th:
-------------------

- Visual inspection, no visibly detached wire bonds, all jumper configs correct for SCC.
- Set 1.8V, 1.0A on channel 1, connected with 4-pin MOLEX cable to chip, and chip's DP1 connected to port A of Ohio card.
- Turning on power to the chip:  
    - Called StartDCS, inital current reads 0.88, and when running CheckDCS, climbs quickly to 1A, then turns off.
    - Then tried manually setting a current limit on channel 1 of 0.85A, set 1.8V on channel, turned it on to have the current limit shut down immeadiately. 
    - Tried the Friday solution of unplugging chip and turning everything (power system, computer) off and on.  Running StartDCS gives a current of 0.512 +/- 0.01 A.  Recieves 1.77V and 1.73V across analog and digital PWR IN pins (1.77 on 1st/right side, 1.73 on 2nd/left side.  Look up channel # later to figure out which is analog and digital). It didn't like this, current went back to ~0.9A.  Again, unplugging chip, turning off computer and power system to see if I get nice 0.51A again.
- Ok, the 'Friday solution' is holding steady so far.  Next step is to run a digital scan and check the output.
- Well ok, now I have a nice current of 0.573A, a perfect Occupancy map (had to run twice), and zero idea of why things broke last week or yesterday.  
- Then I tested the PWR IN with the multimeter, seeing if I could replicate that nasty 0.9A current. I replicated it so well, that even after restarting everything twice, I still have the too-high current instead of the nice 0.51A.  

Did one last restart, called StartDCS to get a 0.9A current, unplugged DisplayPort cable, ran power manually (channel 1, 1.8V, 1.0V, limited current) and got a 0.41A current.  Turned it off, plugged in DisplayPort cable, ran power manually again, got 0.51A current.  Turned it off, and tried with StartDCS and now have a happy current of 0.51A again.  Ask Laura and Ismet for thoughts? Try replicating later?
- Ran a digital scan which was almost perfect but missing 240 pixels, which seemed to be from the same mask loop ([https://gitlab.cern.ch/mglisic/notes/-/blob/master/OccupancyMap_FridaySolution1st.png](url)), and ran it again, this time getting a perfect Occupancy map ([https://gitlab.cern.ch/mglisic/notes/-/blob/master/OccupancyMap_FridaySolution2nd.png](url)).  Current holding at 0.57A.
- Ran analog scan, had a slight smattering of poor pixels, and a bad column in the first third of the chip, but that's ok for pre-tuning. Current at 0.55A.  Will run tuning on the differential FE.
- Ran global threshold tune, with standard threshold target of 1k e-. Current at 0.42 A
- Ran pixel threshold tune, with standard threshold target of 1k e-. Current at 0.42 A again.
- Ran global preamp tuning scan, with mid-range threshold and TOT of 13k e- and 8bc. Current at 0.42 A still.
- Ran pixel threshold tune again, then fine pixel tune scan, current still at 0.42 A. All these tuning scans were only on the differential part of the FE.

Running digital, analog, threshold, tot, and noise scans.  Current is happy 0.42-0.58A
- sCurve scan from the threshold scan, shows a very sharp turn-on curve at appropriate Vcal (100Vcal ~ 1k e-) [https://gitlab.cern.ch/mglisic/notes/-/blob/master/sCurve.png](url)
- Mean TOT map from totscan. Fine agreement with 8bc [https://gitlab.cern.ch/mglisic/notes/-/blob/master/MeanTotMap.png](url)
- Noise scan showed very little noise.




*Lot's of multimeter readings were taken during this session, instructions taken from the testing procedure here (https://travelling-module.readthedocs.io/en/latest/procedure/).*
- *Recieves 1.77V and 1.73V across analog and digital PWR IN pins, though this led to abnormal currentsat least once.  However, upon trying the same procedure, one last time, nothing broke, current stayed at 0.58A.  Not sure why the current was being weird.*
- *IREF_TRIM jumper is in position 3, as in documentation pictures.  We don't have a precise current measuring device on hand, so I'm skipping trimming the IREF here, we had skipped it during the initial set-up too.*
- *Checked VREF_ADC voltage against GND, this made the current jump erratically up to the maxcurrent, and CheckDCS automatically turned it off.  Should have a reading of 0.9V, but the reading wasn't steady enough for me to say.  Try later?*
- *Did try reading VREF_ADC again, got a nice 0.87 V, and this time the current stayed a steady 0.56A*
- *SldoAnalogTrim and SldoDigitalTrim were set at 22, (which is the expected/desired value).  When measured against GND they should read a voltage of 1.2V, I read 1.19 and 1.18V respectively, I'm happy with that.)*



    




